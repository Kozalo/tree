cmake_minimum_required(VERSION 3.10)
project(tree)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -static-libgcc")

add_executable(tree main.cpp)
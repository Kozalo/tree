#include <iostream>

unsigned int determineRowCount(int argc, char* argv[]);
void printSection(unsigned int charCount, unsigned int spaceCount);

int main(int argc, char* argv[]) {
    unsigned int charCount = 1;
    unsigned int rowsCount, rowsLeft, spaceCount;
    rowsCount = rowsLeft = determineRowCount(argc, argv);
    spaceCount = rowsLeft - 1;
    while (rowsLeft > 0) {
        printSection(charCount, spaceCount);
        rowsLeft--, charCount += 2, spaceCount--;
    }
    if (rowsCount == 0) {
        char message[] = "(this is a stump)";
        std::cout << message << std::endl;
        printSection(1, sizeof(message) / 2 - 1);
    } else {
        printSection(1, rowsCount - 1);
    }
    return 0;
}

unsigned int determineRowCount(int argc, char **argv) {
    std::string userInput;
    if (argc > 1) {
        userInput = argv[1];
    } else {
        std::cout << "How tall is the tree? ";
        std::cin >> userInput;
    }

    int rowCount = std::atoi(userInput.c_str());
    return unsigned(abs(rowCount));
}

void printSection(unsigned int charCount, unsigned int spaceCount) {
    std::string spaces(spaceCount, ' ');
    std::string hashes(charCount, '#');
    std::cout << spaces << hashes << std::endl;
}
